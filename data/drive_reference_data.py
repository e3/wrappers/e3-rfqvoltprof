from epics import caput, caget
import numpy as np

device = "RFQ-LLRF1::VoltProf"

data = np.loadtxt("beadpulldata.csv")
caput("{}{}".format(device, "BeadpullZ"), data[:,0])
caput("{}{}".format(device, "BeadpullQ"), data[:,1])
caput("{}{}".format(device, "BeadpullS"), data[:,2])
caput("{}{}".format(device, "BeadpullT"), data[:,3])

Uref = [[ 1.595721e+01,  1.750406e+01,  1.961667e+01,  2.212960e+01,  2.347809e+01],
        [-1.601151e+01, -1.733330e+01, -1.956784e+01, -2.205686e+01, -2.348579e+01],
        [ 1.594112e+01,  1.743993e+01,  1.965892e+01,  2.214157e+01,  2.358938e+01],
        [-1.592073e+01, -1.744011e+01, -1.959686e+01, -2.206076e+01, -2.342759e+01]]
for segment in range(5):
  for pickup in range(4):
    caput("{}PickupRef{}{}".format(device, segment, pickup), Uref[pickup][segment])
