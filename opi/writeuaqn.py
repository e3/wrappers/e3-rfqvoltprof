header='<?xml version="1.0" encoding="UTF-8"?>\n<display version="2.0.0">\n  <name>Display</name>\n  <width>980</width>\n  <height>400</height>\n'
label='  <widget type="label" version="2.0.0">\n    <name>Label</name>\n    <text>{}</text>\n    <x>{}</x>\n    <y>{}</y>\n    <width>{}</width>\n{}  </widget>\n'
footer='</display>'
pvlabel='  <widget type="textupdate" version="2.0.0">\n    <name>Text Update</name>\n    <pv_name>{}</pv_name>\n    <x>{}</x>\n    <y>{}</y>\n    <width>{}</width>\n    <height>{}</height>\n  <precision>2</precision>\n{}  </widget>\n'

boldfont = '    <font>\n      <font family="Liberation Sans" style="BOLD" size="14.0">\n      </font>\n    </font>\n'

pos_x = 10
pos_y = 10

column_spacing = 120
row_spacing = 30
label_width = 110

data_label_width = 35
data_label_spacing = 40

devicename="$(dev)"

with open("uaqn.bob", "w") as f:
  f.write(header)
  for i in range(4):
    f.write(label.format("Quadrant {}".format(i), pos_x, pos_y + 1.5*row_spacing + 2*row_spacing*i, label_width, boldfont))

  for seg in range(5):
    f.write(label.format("Segment {}".format(seg), pos_x + column_spacing*(seg+1), pos_y, label_width, boldfont))
    for quadrant in range(4):
      f.write(pvlabel.format("{}Pickup{}{}".format(devicename, seg, quadrant),   pos_x + column_spacing*(seg+1) + 1*data_label_spacing, pos_y + row_spacing*(2*quadrant+1), data_label_width, 20, boldfont))
      f.write(pvlabel.format("{}PickupIn{}{}".format(devicename, seg, quadrant), pos_x + column_spacing*(seg+1) + 0*data_label_spacing, pos_y + row_spacing*(2*quadrant+2), data_label_width, 20, ""))
      f.write(pvlabel.format("{}PickupCal{}{}".format(devicename, seg, quadrant),   pos_x + column_spacing*(seg+1) + 1*data_label_spacing, pos_y + row_spacing*(2*quadrant+2), data_label_width, 20, ""))
      f.write(pvlabel.format("{}PickupRef{}{}".format(devicename, seg, quadrant),   pos_x + column_spacing*(seg+1) + 2*data_label_spacing, pos_y + row_spacing*(2*quadrant+2), data_label_width, 20, ""))

  f.write(footer)
