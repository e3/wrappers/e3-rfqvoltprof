#
#  Copyright (c) 2021    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : danielnoll
#		    gabrielfedel
# email   : danielnoll@esss.se
# 			gabrielfedel@ess.eu
# Date    : 2020Jan24-0310-40EST
# version : 1.0.0 
#
# 

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500

APP:=rfqvoltprofApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src


USR_CPPFLAGS += -Wall

TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard $(APPDB)/*.substitutions)


SOURCES += $(APPSRC)/rfqvoltprof.cpp
SOURCES += $(APPSRC)/rfqvoltprofstaticdata.cpp

DBDS += $(APPSRC)/rfqvoltprof.dbd

## SYSTEM LIBS 
USR_LIBS += gsl

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

.PHONY: 

vlibs:

.PHONY: vlibs
