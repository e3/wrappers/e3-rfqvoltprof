require common
epicsEnvSet("IOCNAME", "RFQ-010_Ctrl-IOC-02")
epicsEnvSet("AS_TOP", "/opt/nonvolatile/")
iocshLoad("$(common_DIR)/e3-common.iocsh") 

require rfqvoltprof
dbLoadTemplate("rfqvoltprof.substitutions","dev=RFQ-010:EMR-Cav-001:,prefix=VoltProf,debug=0,asize=477")
iocInit()
