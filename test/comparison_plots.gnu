set terminal postscript enhanced color eps size 7cm,4cm

set style line 1 lc rgb "black" lw 3
set style line 2 lc rgb "green" lw 2

set xlabel "z [m]"

set key top horizontal outside

set ylabel "Q"
set output "comparison_Q.eps"
plot "< bash -c 'paste <(grep ReconstructedAbscissa -A 477 esspuconfig_2020_03_10_wrong.txt) <(grep ReconstructedVREF -A 477 esspuconfig_2020_03_10_wrong.txt)'" u 1:2 w l ls 1 title "CEA data", \
     "< bash -c 'paste <(caget -t RFQ-010:VOLTAGEPROFILE:PROFILE.VALD | tr \" \" \"\n\" | tail -n +2) <(caget -t RFQ-010:VOLTAGEPROFILE:PROFILE.VALE | tr \" \" \"\n\" | tail -n +2)'" u 1:2 w l ls 2 title "From EPICS"

set ylabel "S"
set output "comparison_S.eps"
plot "< bash -c 'paste <(grep ReconstructedAbscissa -A 477 esspuconfig_2020_03_10_wrong.txt) <(grep ReconstructedVREF -A 477 esspuconfig_2020_03_10_wrong.txt)'" u 1:3 w l ls 1 title "CEA data", \
     "< bash -c 'paste <(caget -t RFQ-010:VOLTAGEPROFILE:PROFILE.VALD | tr \" \" \"\n\" | tail -n +2) <(caget -t RFQ-010:VOLTAGEPROFILE:PROFILE.VALF | tr \" \" \"\n\" | tail -n +2)'" u 1:2 w l ls 2 title "From EPICS"

set ylabel "T"
set output "comparison_T.eps"
plot "< bash -c 'paste <(grep ReconstructedAbscissa -A 477 esspuconfig_2020_03_10_wrong.txt) <(grep ReconstructedVREF -A 477 esspuconfig_2020_03_10_wrong.txt)'" u 1:4 w l ls 1 title "CEA data", \
     "< bash -c 'paste <(caget -t RFQ-010:VOLTAGEPROFILE:PROFILE.VALD | tr \" \" \"\n\" | tail -n +2) <(caget -t RFQ-010:VOLTAGEPROFILE:PROFILE.VALG | tr \" \" \"\n\" | tail -n +2)'" u 1:2 w l ls 2 title "From EPICS"
