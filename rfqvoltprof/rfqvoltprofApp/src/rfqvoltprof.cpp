#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include <dbDefs.h>
#include <dbCommon.h>
#include <recSup.h>
#include <aSubRecord.h>
#include <subRecord.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include "rfqvoltprof.h"
#include "rfqvoltprofstaticdata.h"

gsl_matrix *pickup_reference, *pickup_calibration;
gsl_matrix *galpha_Q, *galpha_S, *galpha_T, *gtransformation_QSTO;
gsl_matrix *gbasis_Q, *gbasis_S, *gbasis_T;
bool is_initialized = false;

/*
 * Create a new gsl_matrix and initialize with data from array
 */
template <typename D>
static gsl_matrix* new_matrix(D matrix, int N0, int N1)
{
  gsl_matrix* m = gsl_matrix_alloc(N0, N1);
  if(m == NULL)
    printf("Allocation failure.\n");

  for(int i = 0; i < N0; ++i) {
    for(int j = 0; j < N1; ++j) {
      gsl_matrix_set(m,i,j,matrix[i][j]);
    }
  }
  return m;
}

/*
 * Initialize all the gsl_matrix from constant data
 */
static long rfqvoltprof_init(aSubRecord *pasub)
{
  if(!is_initialized) {
    galpha_Q = new_matrix(alpha_Q, Nsegment, Nspectral);
    galpha_S = new_matrix(alpha_S, Nsegment, Nspectral);
    galpha_T = new_matrix(alpha_T, Nsegment, Nspectral);
    gtransformation_QSTO = new_matrix(transformation_QSTO, 4, 4);

    gbasis_Q = gsl_matrix_alloc(Nbasis, Nspectral);
    gbasis_S = gsl_matrix_alloc(Nbasis, Nspectral);
    gbasis_T = gsl_matrix_alloc(Nbasis, Nspectral);
    for(uint row = 0; row < Nbasis; ++row) {
      for(uint col = 0; col < Nspectral; ++col) {
        gsl_matrix_set(gbasis_Q, row, col, basis_function_Q[row][col]);
        gsl_matrix_set(gbasis_S, row, col, basis_function_S[row][col]);
        gsl_matrix_set(gbasis_T, row, col, basis_function_T[row][col]);
      }
    }
    is_initialized = true;
  }

  return 0;
}

/*
 * Transform readings from the 20 pickups to the quadrupole/dipole functions
 *  along the RFQ
 *
 *  Input: in pairs of 4 (input A-D, E-H, ...) the calibrated values of the 
 *    RFQ pickups
 *
 *  Output:
 *   A - C: each a array of 5 double values - coefficients for each of 
 *    the first 5 modes quadrupole Q / dipole S / dipole T normalized 
 *    to the coefficient of the first quadrupolar mode
 *   D: positions of the values in E - G along the RFQ axis z
 *   E - G: quadrupole function Q(z) / dipole function S(z) and T(z) 
 *    reconstructed from the mode coefficients
 */
static long rfqvoltprof_pickup_to_spectral(aSubRecord *psr)
{
  // Check correct dimensioning of the output arrays
  if(psr->nova != 5 || psr->novb != 5 || psr->novc != 5)
    return 1;
  if(psr->novd != Nbasis || psr->nove != Nbasis || 
     psr->novf != Nbasis || psr->novg != Nbasis)
    return 1;

  double signals[4][5] = {
    { *(double*)psr->a, *(double*)psr->e, *(double*)psr->i, *(double*)psr->m, *(double*)psr->q },
    { *(double*)psr->b, *(double*)psr->f, *(double*)psr->j, *(double*)psr->n, *(double*)psr->r },
    { *(double*)psr->c, *(double*)psr->g, *(double*)psr->k, *(double*)psr->o, *(double*)psr->s },
    { *(double*)psr->d, *(double*)psr->h, *(double*)psr->l, *(double*)psr->p, *(double*)psr->t },
  };
  gsl_matrix *pickup = new_matrix(signals, Nquadrant, Nsegment);

  // Transformation to QSTO basis
  gsl_matrix *QSTO = gsl_matrix_alloc(Nquadrant, Nsegment);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, gtransformation_QSTO, pickup, 0, QSTO);

  // Spectral coefficients
  gsl_vector_view Q = gsl_matrix_row(QSTO, 0);
  gsl_vector_view S = gsl_matrix_row(QSTO, 1);
  gsl_vector_view T = gsl_matrix_row(QSTO, 2);

  gsl_vector *c_Q = gsl_vector_alloc(Nspectral);
  gsl_vector *c_S = gsl_vector_alloc(Nspectral);
  gsl_vector *c_T = gsl_vector_alloc(Nspectral);
  gsl_blas_dgemv(CblasTrans, 1, galpha_Q, &Q.vector, 0, c_Q);
  gsl_blas_dgemv(CblasTrans, 1, galpha_S, &S.vector, 0, c_S);
  gsl_blas_dgemv(CblasTrans, 1, galpha_T, &T.vector, 0, c_T);

  // Normalize coefficients to c_Q[0]
  double norm = 1 / gsl_vector_get(c_Q, 0);
  gsl_vector *c_Q_norm = gsl_vector_alloc(Nspectral);
  gsl_vector *c_S_norm = gsl_vector_alloc(Nspectral);
  gsl_vector *c_T_norm = gsl_vector_alloc(Nspectral);
  gsl_vector_set_zero(c_Q_norm); gsl_blas_daxpy(norm, c_Q, c_Q_norm);
  gsl_vector_set_zero(c_S_norm); gsl_blas_daxpy(norm, c_S, c_S_norm);
  gsl_vector_set_zero(c_T_norm); gsl_blas_daxpy(norm, c_T, c_T_norm);
  
  // Scaling of basis functions according to the c_*_norm
  gsl_vector *b_Q = gsl_vector_alloc(Nbasis);
  gsl_vector *b_S = gsl_vector_alloc(Nbasis);
  gsl_vector *b_T = gsl_vector_alloc(Nbasis);
  gsl_blas_dgemv(CblasNoTrans, 1, gbasis_Q, c_Q_norm, 0, b_Q);
  gsl_blas_dgemv(CblasNoTrans, 1, gbasis_S, c_S_norm, 0, b_S);
  gsl_blas_dgemv(CblasNoTrans, 1, gbasis_T, c_T_norm, 0, b_T);

  // Outputs A - C
  double *out_cq = (double *)psr->vala;
  double *out_cs = (double *)psr->valb;
  double *out_ct = (double *)psr->valc;
  for(uint i = 0; i < 5; ++i) {
    out_cq[i] = gsl_vector_get(c_Q_norm, i);
    out_cs[i] = gsl_vector_get(c_S_norm, i);
    out_ct[i] = gsl_vector_get(c_T_norm, i);
  }

  // Outputs D - G
  double *out_z = (double *)psr->vald;
  double *out_q = (double *)psr->vale;
  double *out_s = (double *)psr->valf;
  double *out_t = (double *)psr->valg;
  for(uint i = 0; i < Nbasis; ++i) {
    out_z[i] = basis_function_z[i];
    out_q[i] = gsl_vector_get(b_Q, i);
    out_s[i] = gsl_vector_get(b_S, i);
    out_t[i] = gsl_vector_get(b_T, i);
  }

  // Free space of temporary variables
  gsl_matrix_free(pickup);
  gsl_matrix_free(QSTO);
  gsl_vector_free(c_Q);
  gsl_vector_free(c_S);
  gsl_vector_free(c_T);
  gsl_vector_free(c_Q_norm);
  gsl_vector_free(c_S_norm);
  gsl_vector_free(c_T_norm);
  gsl_vector_free(b_Q);
  gsl_vector_free(b_S);
  gsl_vector_free(b_T);

  return 0;
}

/*
 * Calculates the absolute difference of two functions, defined by
 *  two arrays each: position and value. Interpolates the first array
 *  to the positions of the secondary.
 * 
 * Fails if the sizes of input A and B differs, or if the sizes of 
 *  C and D differ.
 * 
 * Input:
 *  A: array of positions of array #1
 *  B: array of values of array #1
 *  C: array of positions of array #2
 *  D: array of values of array #2
 * 
 * Output:
 *  A: Output values 
 * 
 */
static long rfqvoltprof_difference(aSubRecord *psr)
{
  // Check that position/values arrays have equal sizes for both input
  if(psr->noa != psr->nob || psr->noc != psr->nod)
    return 1;
  // Check that an equal number of values has actually been set on 
  //  both input arrays
  if(psr->nea != psr->neb || psr->nec != psr->ned)
    return 1;

  // First input curve
  double *z1  = (double *)psr->a;
  double *v1  = (double *)psr->b;
  uint    N1  = psr->nea;
 
  // Second input curve
  double *z2 = (double *)psr->c;
  double *v2 = (double *)psr->d;
  uint    N2 = psr->nec;

  // Check that the output has enough space
  if(psr->nova < N2)
    return 1;

  // Loop through second array and calculate the difference to the values
  //  defined by the first curve, interpolating to the position of the second
  uint i1 = 0;  
  double *out = (double *)psr->vala;
  for(uint i2 = 0; i2 < N2; ++i2) {
    // Find position in first array where: z1[i1] < z2[i2] < z1[i1+1]
    while(i1 < (N1 - 2) && z2[i2] > z1[i1+1])
      ++i1;

    // Interpolate values from first array to z2[i2]
    double gx = (z1[i1+1] - z2[i2]) / (z1[i1+1] - z1[i1]);
    double v  = gx * v1[i1] + (1 - gx) * v1[i1+1];

    // Write difference to output array
    out[i2] = v2[i2] - v;
  }

  // Output array has the size of the second array
  psr->neva = N2;

  return 0;
}

// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(rfqvoltprof_init);
epicsRegisterFunction(rfqvoltprof_pickup_to_spectral);
epicsRegisterFunction(rfqvoltprof_difference);