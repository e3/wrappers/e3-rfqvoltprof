#include <aSubRecord.h>

#include <gsl/gsl_matrix.h>

template <typename D>
static gsl_matrix* new_matrix(D matrix, int N0, int N1);

static long rfqvoltprof_init(aSubRecord *pasub);
static long rfqvoltprof_pickup_to_spectral(aSubRecord *psr);
static long rfqvoltprof_difference(aSubRecord *psr);