// Number of RFQ segments
extern const long Nsegment;

// Number of quadrants (well... 4)
extern const long Nquadrant;

// Number of spectral coefficients calculated
extern const long Nspectral;

/* Table containing the tabulated RFQ quadrupole and dipole
 *  basis functions provided by CEA as a function of 
 *  position along the RFQ. See ESS-TODO.
 */
// Number of points along RFQ axis
extern const long Nbasis;
// Location of the points along RFQ axis
extern const double basis_function_z[];   // [Nbasis]
// Quadrupole function Q(z) for each mode
extern const double basis_function_Q[][5]; // [Nbasis][Nspectral]
// Dipole function S(z) for each mode
extern const double basis_function_S[][5]; // [Nbasis][Nspectral]
// Dipole function T(z) for each mode
extern const double basis_function_T[][5]; // [Nbasis][Nspectral]
// Reference Q(z) from bead-pull measurement
extern const double reference_Q[];        // [Nbasis]

// Transformation from four quadrants to quadrupole / dipole functions
extern const double transformation_QSTO[4][4]; // [Nquadrant][Nquadrant]

// Projection of quadrupole / dipole functions to mode spectrum
extern const double alpha_Q[][5];  // [Nsegment][Nspectral]
extern const double alpha_S[][5];  // [Nsegment][Nspectral]
extern const double alpha_T[][5];  // [Nsegment][Nspectral]
