#
# PVs for calibration/reference pickup values.
#  To be set by user/calibration database
#
record(ai, "$(dev)$(prefix)PickupRef$(segment)$(quadrant)") {
  field(DESC, "Bead-pull value for pickup $(segment) $(quadrant)")
  field(PINI, "YES")

  info(autosaveFields, "VAL")
}
record(ai, "$(dev)$(prefix)PickupCal$(segment)$(quadrant)") {
  field(DESC, "Calibration pickup $(segment) $(quadrant)")
  field(PINI, "YES")

  info(autosaveFields, "VAL")
}

#
# Pickup input values
#
record(calc, "$(dev)#$(prefix)PickupInSim$(segment)$(quadrant)") {
  field(DESC, "Pickup values for sim mode")

  # Vary values around calibration values
  field(CALC, "A*($(avg=1)+$(pp=0.01)*(2*RNDM-1))")
  field(INPA, "$(dev)$(prefix)PickupCal$(segment)$(quadrant)")

  field(TPRO, "$(debug=0)")
}

record(sel, "$(dev)$(prefix)PickupIn$(segment)$(quadrant)") {
  field(DESC, "Raw pickup value $(segment) $(quadrant)")

  # Switch between measurement and simulation value
  field(INPA, "RFQ-010:RFS-DIG-10$(dig):AI$(channel)-SMonAvg-Mag")
  field(INPB, "$(dev)#$(prefix)PickupInSim$(segment)$(quadrant) PP")

  field(SELM, "Specified")
  field(NVL,  "$(dev)$(prefix)SimEnable")

  field(TPRO, "$(debug=0)")
}

#
# Acquisition PV: pickup measurement value linearly scaled to the reference measurement
#
record(calc, "$(dev)$(prefix)Pickup$(segment)$(quadrant)") {
  field(DESC, "Calibrated pickup value $(segment) $(quadrant)")

  field(CALC, "C*A/B")
  field(INPA, "$(dev)$(prefix)PickupIn$(segment)$(quadrant) PP")
  field(INPB, "$(dev)$(prefix)PickupCal$(segment)$(quadrant)")
  field(INPC, "$(dev)$(prefix)PickupRef$(segment)$(quadrant)")

  field(PINI, "YES")

  field(TPRO, "$(debug=0)")
}

